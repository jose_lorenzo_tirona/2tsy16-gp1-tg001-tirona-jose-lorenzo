﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz_1___TradingPost
{
    class Program
    {
        struct Order
        {
            public string Name;
            public double Price;
        }

        static List<Order> GetDefaultOrders()
        {
            List<Order> defaultOrders = new List<Order>();

            Order o1 = new Order();
            o1.Name = "Rattlesnake";
            o1.Price = 378999988;
            defaultOrders.Add(o1);

            Order o2 = new Order();
            o2.Name = "Rattlesnake";
            o2.Price = 377577659;
            defaultOrders.Add(o2);

            Order o3 = new Order();
            o3.Name = "Rattlesnake";
            o3.Price = 414999900;
            defaultOrders.Add(o3);

            Order o4 = new Order();
            o4.Name = "Rattlesnake";
            o4.Price = 398800000;
            defaultOrders.Add(o4);

            Order o5 = new Order();
            o5.Name = "Drake";
            o5.Price = 48000000;
            defaultOrders.Add(o5);

            Order o6 = new Order();
            o6.Name = "Drake";
            o6.Price = 51975000;
            defaultOrders.Add(o6);

            Order o7 = new Order();
            o7.Name = "Drake";
            o7.Price = 49999999;
            defaultOrders.Add(o7);

            Order o8 = new Order();
            o8.Name = "PLEX";
            o8.Price = 1180000000;
            defaultOrders.Add(o8);

            Order o9 = new Order();
            o9.Name = "PLEX";
            o9.Price = 1177566613;
            defaultOrders.Add(o9);

            Order o10 = new Order();
            o10.Name = "PLEX";
            o10.Price = 1182899876;
            defaultOrders.Add(o10);

            return defaultOrders;
        }

        // Quiz Item
        // Return a list of items that match your item name
        static List<Order> FilterOrders(List<Order> orders, string filterKey)
        {
            List<Order> filteredOrder = new List<Order>();

            foreach (Order item in orders)
            {
                // if item name == filter key, do filterOrder.Add(item)
                if(filterKey == item.Name)
                {
                    filteredOrder.Add(item);
                }
            }

            return filteredOrder;

        }

        // Quiz Item
        // Return a list of ordered items by price
        static List<Order> SortOrders(List<Order> orders, string filterKey)
        {
            List<Order> filterOrder = FilterOrders(orders, filterKey);
            List<Order> sortedOrder = new List<Order>();
            List<double> prices = new List<double>();
            double price;
            // New list for prices
            foreach (Order item in filterOrder)
            {
                price = item.Price;
                prices.Add(price);
            }
            // Sort descending
            foreach (double num in prices)
            {
                prices.Sort((a, b) => a.CompareTo(b));
            }
            // New list for filtered orders with sorted prices
            foreach (double num in prices)
            {
                foreach (Order item in orders)
                {
                    if(num == item.Price)
                    {
                        sortedOrder.Add(item);
                    }
                }
            }
            return sortedOrder;
        }

        // Quiz Item
        // Sell the item for a value higher than the price given, or a value equal to the price given
        // If there are no buyers that match your order, print "Your item has been listed"
        static void PostTransaction(List<Order> orders, string name, int price)
        {
            int index = 0;
            double defaultBuyer= 0;
            // Use item name to filter order
            // Sort order by price
            List<Order> sortedOrder = SortOrders(orders, name);
            // If price equal to given price, sell. Else, sell to highest bidder
            foreach (Order item in sortedOrder)
            {
                if (price == item.Price)
                {
                    index = 1;
                    break;
                }
                else if (price > item.Price)
                {
                    index = 2;
                    break;
                }
                else
                {
                    defaultBuyer = item.Price;
                }

            }
            if (index == 1)
                Console.WriteLine("Your item has been sold for " + price + ".");
            else if (index == 2)
                Console.WriteLine("Your item has been listed on the market.");
            else
                Console.WriteLine("Your item has been sold for " + defaultBuyer + ".");
        }

        static void Main(string[] args)
        {
            while (true) // Loop Market
            {
                string item;
                int price;
                List<Order> allOrders = GetDefaultOrders();
                Console.Write("Enter the name of the item you want to sell: ");
                item = Console.ReadLine();

                Console.Write("Enter the amount you want to sell the item for: ");
                price = Convert.ToInt32(Console.ReadLine());

                PostTransaction(allOrders, item, price);
                Console.ReadKey();
                Console.Clear(); // Clear Screen
            }
        }
    }
}
