﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_12
{
    class Program
    {
        static void AssignSkills(Character hero)
        {
            // Create skills
            DmgSkill Dmg = new DmgSkill(5);
            HealSkill Heal = new HealSkill(8);
            StealSkill Steal = new StealSkill(2);
            // Call hero.Skills.Add(skill1);
            hero.skills.Add(Dmg);
            hero.skills.Add(Heal);
            hero.skills.Add(Steal);
        }
        static void Main(string[] args)
        {
            int i;
            bool turn = true;
            Random rand = new Random();
            Character hero = new Character("Lemniscate", 100, 10, 5);
            Monster ai = new Monster("Number 8", 65, 7, 3);
            AssignSkills(hero);

            while (ai.CurrHP > 0 && hero.CurrHP > 0)
            {
                hero.DisplayStats();
                ai.DisplayStats();
                
                Console.WriteLine("Skills:\n\t[1]\tDamage Skill\n\t[2]\tHeal Up\n\t[3]\tSteal HP\n");
                //i = Convert.ToInt32(Console.ReadLine());

                if (turn == true)
                {
                    int skill = rand.Next(0,hero.skills.Count);
                    hero.skills[skill].Use(hero, ai);
                }
                else if(turn == false)
                {
                    hero.CurrHP -= ai.Attk*(rand.Next(3));
                }
                Console.ReadKey(); Console.Clear();
                turn = !turn;
            }
            if (ai.CurrHP <= 0) Console.WriteLine("{0} won!", hero.Name);
            else Console.WriteLine("{0} won!", ai.Name);
        }
    }
}
