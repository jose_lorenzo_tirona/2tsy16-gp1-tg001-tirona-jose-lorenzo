﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_12
{
    public class DmgSkill : Skill
    {
        public DmgSkill(int cd) : base (cd)
        { }
        public override void Use(Unit caster, Unit target)
        {
            target.CurrHP -= caster.Attk * (1 + (1 / rand.Next(1,5))) ;
            Console.WriteLine("{0} used Damage!", caster.Name);
            base.Use(caster, target);
        }
    }
}
