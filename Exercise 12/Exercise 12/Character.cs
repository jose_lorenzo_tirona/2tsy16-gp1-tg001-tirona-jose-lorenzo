﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_12
{
    public class Character : Unit
    {
        public Character(string name, float hp, float attk, float def) : base(name, hp, attk, def)
        { }

    }
}
