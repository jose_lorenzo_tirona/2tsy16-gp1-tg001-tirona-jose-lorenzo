﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_12
{
    public class Monster : Unit
    {
        public Monster(string name, float hp, float attk, float def) : base(name, hp, attk, def)
        { }
    }
}
