﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_12
{
    public class StealSkill : Skill
    {
        public StealSkill(int cd) : base (cd)
        { }
        public override void Use(Unit caster, Unit target)
        {
            target.CurrHP -= caster.Attk * (1 + (1 / rand.Next(1,5))) / 2;
            caster.CurrHP += caster.Attk * (1 + (1 / rand.Next(1,5))) / 2;
            Console.WriteLine("{0} used Steal!", caster.Name);
            base.Use(caster, target);
        }
    }
}
