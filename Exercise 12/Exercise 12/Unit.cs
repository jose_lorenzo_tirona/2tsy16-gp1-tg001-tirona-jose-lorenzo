﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_12
{
    public class Unit
    {

        public Unit(string name, float hp, float attk, float def)
        {
            Name = name;
            MaxHP = hp;
            Attk = attk;
            Def = def;
            CurrHP = hp;
        }
        public string Name { get; set; }
        public float MaxHP { get; set; }
        public float Attk { get; set; }
        public float Def { get; set; }
        private float currHP { get; set; }
        public float CurrHP {
            get { return currHP; }

            set
            {
                currHP = value;
                if (currHP > MaxHP) currHP = MaxHP;
                
            }
        }
        
        public List<Skill> skills = new List<Skill>();

        public void DisplayStats()
        {
            Console.WriteLine("{0}\t\tHP: {1}/{2}\tPOW: {3}\tDEF: {4}", Name, CurrHP, MaxHP, Attk, Def);
        }
}
}
