﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_12
{
    public class Skill
    {
        public static readonly Random rand = new Random();
        public string Name;
        public int Cost;
        public int Cooldown;
        public Skill(int cd)
        {
            Cooldown = cd;
        }
        public virtual void Use(Unit caster, Unit target)
        {
            Console.WriteLine("Skill used");
        }

    }
}
