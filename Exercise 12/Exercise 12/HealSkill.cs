﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_12
{
    public class HealSkill: Skill
    {
        public HealSkill(int cd) : base (cd)
        { }
        public override void Use(Unit caster, Unit target)
        {
            caster.CurrHP += rand.Next(5,10);
            Console.WriteLine("{0} used Heal!", caster.Name);
            base.Use(caster, target);
        }
    }
}
