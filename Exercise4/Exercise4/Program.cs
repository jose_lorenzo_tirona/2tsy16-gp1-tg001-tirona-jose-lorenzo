﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise4
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] items = { "Health Potion", "Health Potion", "Health Potion", "Health Potion", "Mana Potion", "Mana Potion", "Mana Potion", "Mana Potion", "Mana Potion", "Yggdrasil Leaf", "Yggdrasil Leaf", "Yggdrasil Leaf", "Dreadwyrm Staff", "Holy Robe", "Holy Robe" };
            Console.WriteLine("Item to search: ");
            string input = Console.ReadLine();
            Quantity(items, input);

        }
        static void Quantity(string[] items, string searchedItem)
        {
            int x = 0;
            for(int i = 0; i < items.Length; i++)
            {
                if (searchedItem == items[i])
                    x++;
            }
            Console.WriteLine("{0} Pieces", x);
        }
    }
}
