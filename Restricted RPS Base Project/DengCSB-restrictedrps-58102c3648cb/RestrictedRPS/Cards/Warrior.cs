﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Warrior : Card
    {
        public Warrior()
        {
            Type = "Warrior";
        }
        public override FightResult Fight(Card opponentCard)
        {
            Card assassin = new Assassin();
            if (opponentCard.Type == this.Type)
                return FightResult.Draw;
            else if (opponentCard.Type == assassin.Type)
                return FightResult.Win;
            else return FightResult.Lose;

        }
    }
}
