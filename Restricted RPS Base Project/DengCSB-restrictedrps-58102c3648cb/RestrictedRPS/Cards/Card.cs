﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public enum FightResult
    {
        Win,
        Lose,
        Draw
    }

    public abstract class Card
    {
        public string Type { get; set; }
        public Card() { }
        /// <summary>
        /// This method should be called upon playing a card against opponent's card.
        /// - Warrior beats Assassin
        /// - Assassin beats Mage
        /// - Mage beats Warrior
        /// - If cards are the same, draw
        /// </summary>
        /// <param name="opponentCard"></param>
        /// <returns>Fight result.</returns>
        public abstract FightResult Fight(Card opponentCard);
    }
}
