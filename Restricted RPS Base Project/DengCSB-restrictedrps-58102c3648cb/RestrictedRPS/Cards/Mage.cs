﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Mage : Card
    {
        public Mage()
        {
            Type = "Mage";
        }
        public override FightResult Fight(Card opponentCard)
        {
            Card warrior = new Warrior();
            if (opponentCard.Type == this.Type)
                return FightResult.Draw;
            else if (opponentCard.Type == warrior.Type)
                return FightResult.Win;
            else return FightResult.Lose;
        }
    }
}
