﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Assassin : Card
    {
        public Assassin()
        {
            Type = "Assassin";
        }
        public override FightResult Fight(Card opponentCard)
        {
            Card mage = new Mage();
            if (opponentCard.Type == this.Type)
                return FightResult.Draw;
            else if (opponentCard.Type == mage.Type)
                return FightResult.Win;
            else return FightResult.Lose;
        }
    }
}
