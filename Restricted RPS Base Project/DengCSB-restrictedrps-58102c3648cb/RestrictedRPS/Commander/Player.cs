﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Player : Commander
    {
        /// <summary>
        /// Get player's input. Player can only either Play or Discard.
        /// </summary>
        public override void EvaluateTurn(Commander opponent)
        {
            Console.WriteLine("\n[1] - Play\n[2] - Discard\n");

            int move = Convert.ToInt32(Console.ReadLine());
            switch (move)
            {
                case 1:
                    Fight(opponent);
                    break;
                case 2:
                    Cards.Add(Discard());
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Show a list of cards to choose from. If there are 2 cards of the same type (eg. Warrior), only show one of each type.
        /// </summary>
        /// <returns></returns>
        public override Card PlayCard()
        {
            Console.WriteLine("\nChoose a card to play:\n");
            DisplayCards();
            int move = Convert.ToInt32(Console.ReadLine());
            
            return Available[move-1];
        }
    }
}
