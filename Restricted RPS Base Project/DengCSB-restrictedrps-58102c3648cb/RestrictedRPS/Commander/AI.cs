﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class AI : Commander
    {
        /// <summary>
        /// 30% chance to discard, 70% chance to play a card.
        /// </summary>
        public override void EvaluateTurn(Commander opponent)
        {
            if (RandomHelper.Chance(0.3f))
                Cards.Add(Discard());
            else
            {
                Console.WriteLine(Name + " chose to fight.");
                Fight(opponent);
            }
        }

        public override Card PlayCard()
        {
            int rand = RandomHelper.Range(Cards.Count);
            return Cards[rand];
        }
    }
}
