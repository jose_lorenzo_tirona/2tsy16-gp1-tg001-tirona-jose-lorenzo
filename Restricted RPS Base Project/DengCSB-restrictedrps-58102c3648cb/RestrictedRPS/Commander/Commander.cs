﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public abstract class Commander
    {
        public string Name { get; set; }
        public int Points { get; protected set; }

        private List<Card> cards = new List<Card>();

        public List<Card> Available = new List<Card>();
        public IList<Card> Cards { get { return cards; } }
        public bool EmptyHand { get { return cards.Count == 0; } }

        /// <summary>
        /// Can only discard if commander has at least 2 cards.
        /// </summary>
        public bool CanDiscard
        {
            get
            {
                if (cards.Count < 2) return false;
                else return true;
            }
        }

        /// <summary>
        /// Draw a random card and add it to your hand.
        /// </summary>
        /// <returns></returns>
        public Card Draw()
        {
            int rand = RandomHelper.Range(1, 3);
            switch(rand)
            {
                case 1:
                    return new Warrior();
                case 2:
                    return new Assassin();
                default:
                    return new Mage();
            }
        }

        /// <summary>
        /// Discards two random cards in exchange of one random card. Cannot discard if player has only one card (throws an exception)
        /// </summary>
        /// <returns>Received card after discarding.</returns>
        public Card Discard()
        {
            if (!CanDiscard) throw new Exception("Cannot discard now."); // DON'T DELETE THIS LINE
            else
            {
                Console.WriteLine(Name + " discarded.");
                for(int x = 0; x < 2; x++)
                    Cards.RemoveAt(RandomHelper.Range(cards.Count));
                return Draw();
            }            
        }

        /// <summary>
        /// Display this commander's cards. NOTE: Only call this for the player's turn.
        /// </summary>
        public void DisplayCards()
        {
            Card assassin = new Assassin();
            Card mage = new Mage();
            Card warrior = new Warrior();

            foreach (Card card in cards)
            {
                bool exists = false;
                foreach (Card choices in Available)
                    if (card.Type == choices.Type)
                        exists = true;
                if (exists == false)
                    Available.Add(card);
            }

            foreach (Card card in Available)
                Console.WriteLine(Available.IndexOf(card)+1 + " - " + card.Type);
        }

        /// <summary>
        /// Called whenever one side's hand is empty. All cards will be discarded. Each discarded card will reduce the player's point by 1.
        /// </summary>
        public void OnGameEnding()
        {
            Points -= cards.Count();
        }

        /// <summary>
        /// Commander's action upon his turn. Can either "Play" or "Discard"
        /// </summary>
        /// <param name="opponent"></param>
        public abstract void EvaluateTurn(Commander opponent);

        /// <summary>
        /// Choose a card to play. Card must be discarded after playing.
        /// </summary>
        /// <returns>Card to play.</returns>
        public abstract Card PlayCard();

        /// <summary>
        /// Each commander plays a card. Points are evaluated here.
        /// Free code :)
        /// </summary>
        /// <param name="opponent"></param>
        public void Fight(Commander opponent)
        {
            Card myCard = PlayCard();
            Card opponentCard = opponent.PlayCard();

            Console.WriteLine(Name + "'s " + myCard.Type + " vs " + opponent.Name + "'s " + opponentCard.Type);

            FightResult result = myCard.Fight(opponentCard);
            if (result == FightResult.Win)
            {
                Points += 2;
                Console.WriteLine(Name + " wins.");
            }
            else if (result == FightResult.Lose)
            {
                Console.WriteLine(Name + " lost.");
            }
            else
                Console.WriteLine("Draw.");
            //Remove user's card
            foreach (Card card in cards)
            {
                if (myCard.Type == card.Type)
                {
                    Cards.Remove(card);
                    break;
                }
            }
            //Remove opponent's card
            foreach (Card card in opponent.cards)
            {
                if (opponentCard.Type == card.Type)
                {
                    opponent.Cards.Remove(card);
                    break;
                }
            }
            //Clear available
            Available.Clear();
        }
    }
}
