﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            bool turn = false;
            Unit hero = new Unit("Sir Loin", 999.0f, 3.1415265f*13);
            Unit monster = new Unit("Butcher Knife", 250.0f, 23.0f);

            Console.WriteLine("The battle between Sir Loin and the Butcher Knife begins.");
            while (hero.changeHP > 0 && monster.changeHP > 0)
            {
                Console.Clear();
                if (turn == false)
                {
                    Console.WriteLine(monster.Name + "'s Turn! \n==============================");
                    hero.changeHP -= monster.Damage;
                    Console.WriteLine(hero.Name + " lost " + monster.Damage + "HP. Current HP: " + hero.changeHP);
                    turn = true;
                }
                else
                {
                    Console.WriteLine(hero.Name + "'s Turn! \n==============================");
                    monster.changeHP -= hero.Damage;
                    Console.WriteLine(monster.Name + " lost " + hero.Damage + "HP. Current HP: " + monster.changeHP);
                    turn = false;
                }

                Console.ReadKey();
            }
            Console.Clear();
            if (monster.changeHP > 0)
                Console.WriteLine(hero.Name + " died.\n" + monster.Name + " is the winner!\n");
            else
                Console.WriteLine(monster.Name + " died.\n" + hero.Name + " is the winner!\n");
        }
    }
}
