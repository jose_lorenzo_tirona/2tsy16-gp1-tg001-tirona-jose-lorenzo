﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Unit
    {
        public Unit(string name, float hp, float atk)
        {
            Name = name;
            CurrentHP = hp;
            MaxHP = hp;
            Damage = atk;
        }
        public string Name;
        private float CurrentHP;
        public float changeHP
        {
            get
            {
                return CurrentHP;
            }
            set
            {
                CurrentHP = value;
                if (CurrentHP < 0)
                    CurrentHP = 0;
            }
        }
        private float MaxHP;
        public float Damage;
    }
}
