﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_5
{
    class Program
    {
        static void Main(string[] args)
        {
            int low= 9999;
            //int index = 0;
            List<int> sorted = new List<int>();
            List<int> hp = new List<int>();
            hp.Add(99);
            hp.Add(169);
            hp.Add(77);
            hp.Add(58);
            hp.Add(150);
            hp.Add(1);
            hp.Add(187);
            hp.Add(999);
            hp.Add(204);
            hp.Add(386);
            hp.Add(22);
            hp.Add(123);
            int high = hp[0];
            foreach (int num in hp)
            {
                hp.Sort((a, b) => a.CompareTo(b)); // ascending sort
                hp.Sort((a, b) => -1 * a.CompareTo(b)); // descending sort
            }
            sorted = hp;
            //Console.WriteLine(high);
            foreach (int num in sorted)
                Console.WriteLine(num);
        }
    }
}
