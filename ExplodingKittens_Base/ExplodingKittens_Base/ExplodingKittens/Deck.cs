﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class Deck
    {
        private Random random = new Random();

        public IEnumerable<Card> CardPile
        {
            get { return cardPile; }
        }
        private Stack<Card> cardPile = new Stack<Card>();
        private List<Card> discardPile = new List<Card>();

        /// <summary>
        /// Deal 4 cards to every participant from the cardPile
        /// </summary>
        /// <param name="participants"></param>
        public void DealCards(ICollection<Participant> participants)
        {
            Card defuse = new Defuse();
            // Make a new cardPile from CreateDealDeck()
            CreateDealDeck();
            // Deal cards for every participant
            // Deal 4 cards
            // Add 1 defuse card
            foreach (Participant participant in participants)
            {
                for (int x = 0; x < 4; x++)
                {
                    participant.Hand.Add(cardPile.Pop());
                }
                participant.Hand.Add(defuse);
            }
        }

        /// <summary>
        /// Completes the deck by adding exploding kittens, defuse cards and shuffling the deck
        /// </summary>
        public void CompleteSetup()
        {
            Card boom = new ExplodingKitten();
            Card defuse = new Defuse();
            // Add Exploding Kittens and Defuse cards to complete the deal
            for (int x = 0; x < 4; x++)
                cardPile.Push(boom);
            for (int x = 0; x < 2; x++)
                cardPile.Push(defuse);
            // Shuffle the cards after adding exploding kittens and defuse cards
            Shuffle();
        }

        public void Shuffle()
        {
            List<Card> temp = new List<Card>();
            temp.AddRange(cardPile);
            cardPile.Clear();

            int x = temp.Count;
            while (x > 1)
            {
                x--;
                int y = random.Next(x + 1);
                Card tmp = temp[y];
                temp[y] = temp[x];
                temp[x] = tmp;
            }
            
            foreach (Card item in temp)
            {
                cardPile.Push(item);
            }
        }

        public Card DrawCard()
        {
            // When the carePile reaches 0 cards, take the discard pile
            // Shuffle it and make it the new cardPile

                // Empty the discardPile after making the new cardPile
            if(cardPile.Count() < 1)
            {
                foreach(Card card in discardPile)
                {
                    AddCard(card, random.Next(cardPile.Count));
                }
                discardPile.Clear();
            }

            return cardPile.Pop();
        }

        public bool AddCard(Card card, int index)
        {
            if (index > cardPile.Count)
                return false;
            List<Card> temp = new List<Card>();
            temp.AddRange(cardPile);

            temp.Insert(index, card);

            foreach(Card item in temp)
            {
                cardPile.Push(item);
            }

            return true;
        }

        public void AddDiscardedCard(Card card)
        {
            discardPile.Add(card);
        }

        /// <summary>
        /// Returns an array of all playing cards EXCEPT ExplodingKitten and Defuse
        /// </summary>
        /// <returns></returns>
        private IEnumerable<Card> CreateDealDeck()
        {
            Card attack = new Attack();
            Card favor = new Favor();
            Card stf = new SeeTheFuture();
            Card shuffle = new Shuffle();
            Card skip = new Skip();
            Card taco = new CatCard(CatCard.CardType.TacoCat);
            Card rainbow = new CatCard(CatCard.CardType.RainbowCat);
            Card melon = new CatCard(CatCard.CardType.MelonCat);
            Card bread = new CatCard(CatCard.CardType.BeardCat);
            Card potato = new CatCard(CatCard.CardType.HairyPotatoCat);

            List<Card> cards = new List<Card>();
            cards.Add(attack);
            cards.Add(favor);
            cards.Add(skip);
            cards.Add(shuffle);
            cards.Add(taco);
            cards.Add(rainbow);
            cards.Add(potato);
            cards.Add(melon);
            cards.Add(bread);

            foreach (Card card in cards)
            {
                for (int x = 0; x < 4; x++)
                    cardPile.Push(card);
            }
            for (int x = 0; x < 5; x++)
                cardPile.Push(stf);
            Shuffle();
            // Setup unique card allocation
            return cardPile;
        }
    }
}
