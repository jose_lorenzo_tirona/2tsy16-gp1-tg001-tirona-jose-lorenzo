﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class Skip : Card
    {
        public Skip()
        {
            CanUseManually = true;
        }

        public override void DisplayCard()
        {
            Console.WriteLine("Skip Cat");
        }

        public override void Use(Participant actor, ICollection<Participant> participants, IList<Participant> turnOrder, Deck deck)
        {
            base.Use(actor, participants, turnOrder, deck);
            Console.WriteLine(actor.Name + " used Skip. Next player please!");

            actor.ShouldDraw = false;
            actor.TurnEnded = true;
            actor.Hand.Remove(this);
            deck.AddDiscardedCard(this);
            // Set the actor's ShouldDraw to false
            // and TurnEnded to true
        }
    }
}
