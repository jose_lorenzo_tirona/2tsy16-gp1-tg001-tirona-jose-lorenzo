﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class SeeTheFuture : Card
    {
        public SeeTheFuture()
        {
            CanUseManually = true;
        }

        public override void DisplayCard()
        {
            Console.WriteLine("See The Future Cat");
        }

        public override void Use(Participant actor, ICollection<Participant> participants, IList<Participant> turnOrder, Deck deck)
        {
            base.Use(actor, participants, turnOrder, deck);
            Console.WriteLine(actor.Name + " used See The Future Cat on the deck!");
            // Print the top 3 cards of the deck
            for(int x = 0; x < 3; x++)
            {
                deck.CardPile.ElementAt(x).DisplayCard();
            }
            actor.Hand.Remove(this);
            deck.AddDiscardedCard(this);
        }
    }
}
