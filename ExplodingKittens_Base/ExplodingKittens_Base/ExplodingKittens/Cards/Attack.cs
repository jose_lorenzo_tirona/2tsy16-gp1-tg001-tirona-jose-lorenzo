﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class Attack : Card
    {
        public Attack()
        {
            CanUseManually = true;
        }

        public override void DisplayCard()
        {
            Console.WriteLine("Attack Cat");
        }

        /// <summary>
        /// Skip the current actor and the next Participant in the turn order will have double their turn
        /// </summary>
        /// <param name="actor"></param>
        /// <param name="participants"></param>
        /// <param name="turnOrder"></param>
        /// <param name="deck"></param>
        public override void Use(Participant actor, ICollection<Participant> participants, IList<Participant> turnOrder, Deck deck)
        {
            base.Use(actor, participants, turnOrder, deck);

            // Set the next Participant's NumTurns to 2 (first in the turnOrder)
            turnOrder.First().NumTurns = 2;
            actor.NumTurns = 0;
            actor.Hand.Remove(this);
            deck.AddDiscardedCard(this);
        }
    }
}
