﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class ExplodingKitten : Card
    {
        public ExplodingKitten()
        {
            CanUseManually = true;
        }

        public override void DisplayCard()
        {
            Console.WriteLine("BOOM! Cat");
        }

        public override void Use(Participant actor, ICollection<Participant> participants, IList<Participant> turnOrder, Deck deck)
        {
            base.Use(actor, participants, turnOrder, deck);
            // Actor died, set isAlive to false
            // Call OnCardUsing on each card in the actor's hand
            // If a card has responded, stop the loop and remove that card from the hand
            Console.WriteLine("Oh no! An Exploding Kitten! I hope you have a defuse card.");

            actor.IsAlive = false;
            foreach(Card card in actor.Hand)
            {
                if(card.OnCardUsing(this, actor, participants, turnOrder, deck) is Defuse)
                {
                    actor.Hand.Remove(this);
                    break;
                }
            }
        }
    }
}
