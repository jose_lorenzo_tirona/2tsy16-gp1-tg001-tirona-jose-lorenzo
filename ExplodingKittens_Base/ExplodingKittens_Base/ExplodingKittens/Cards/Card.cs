﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    abstract class Card
    {
        protected Random random = new Random();

        // Dictates if the card can be selected and used manually.
        public bool CanUseManually
        {
            get; protected set;
        }

        public virtual void Use(Participant actor, ICollection<Participant> participants, IList<Participant> turnOrder, Deck deck)
        {
            // DO NOT DELETE THIS LINE
            if (!CanUseManually) throw new Exception("Cannot use card that cannot be used manually");
        }

        /// <summary>
        /// Used to add/negate any effects that the current card may have
        /// </summary>
        /// <param name="card"></param>
        /// <param name="actor"></param>
        /// <param name="participants"></param>
        /// <param name="turnOrder"></param>
        /// <param name="deck"></param>
        /// <returns>The card that was used in response to the current card. Null if the card didn't react</returns>
        public virtual Card OnCardUsing(Card card, Participant actor, ICollection<Participant> participants, IList<Participant> turnOrder, Deck deck)
        {
            return null;
        }

        public abstract void DisplayCard();
    }
}
