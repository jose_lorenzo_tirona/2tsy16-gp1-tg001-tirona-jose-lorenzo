﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class Favor : Card
    {
        public Favor()
        {
            CanUseManually = true;
        }

        public override void DisplayCard()
        {
            Console.WriteLine("Favor Cat");
        }

        public override void Use(Participant actor, ICollection<Participant> participants, IList<Participant> turnOrder, Deck deck)
        {
            base.Use(actor, participants, turnOrder, deck);

            // Call SelectTarget from the actor
            // Check if that target still has cards
            // Randomly steal a card if they still have a card
            Console.WriteLine(actor.Name + " used Favor!");
            actor.SelectTarget(participants);
            if (actor.SelectTarget(participants).Hand.Count < 0)
                CanUseManually = false;
            Card temp = actor.SelectTarget(participants).Hand.ElementAt(random.Next(0, actor.SelectTarget(participants).Hand.Count));
            actor.Hand.Add(temp);
            actor.SelectTarget(participants).Hand.Remove(temp);
            actor.Hand.Remove(this);
            deck.AddDiscardedCard(this);
        }
    }
}
