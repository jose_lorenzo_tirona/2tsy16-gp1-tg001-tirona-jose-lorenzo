﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class CatCard : Card
    {
        public enum CardType
        {
            TacoCat,
            RainbowCat,
            MelonCat,
            BeardCat,
            HairyPotatoCat
        }

        public CardType CatType { get; private set; }

        public CatCard(CardType type)
        {
            CatType = type;
            CanUseManually = true;
        }

        public override void DisplayCard()
        {
            Console.WriteLine(CatType);
        }

        /// <summary>
        /// Check if the actor has atleast 2 cat cards
        /// </summary>
        /// <param name="actor"></param>
        /// <param name="participants"></param>
        /// <param name="turnOrder"></param>
        /// <param name="deck"></param>
        public override void Use(Participant actor, ICollection<Participant> participants, IList<Participant> turnOrder, Deck deck)
        {
            base.Use(actor, participants, turnOrder, deck);
             
            // Special condition, check if the actor has atleast 1 other cat card of the same ty
                // If they can't use this cat card, return the card to the actor and let them select again
            foreach(Card card in actor.Hand)
            {
                if(card is CatCard)
                {

                }
            }    
            // If they can, grant a Blind Pick
                // Remove one additional cat card w/ the same type after stealing
                // Remember to add that card to the discard pile
            if(true)
            {
                foreach(Card card in actor.Hand)
                {
                    //if(card is CatCard)
                }
            }
            throw new NotImplementedException();
        }
    }
}
