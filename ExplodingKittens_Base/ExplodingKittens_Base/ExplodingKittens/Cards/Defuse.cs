﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class Defuse : Card
    {
        public Defuse()
        {
            CanUseManually = false;
        }

        public override void DisplayCard()
        {
            Console.WriteLine("Defuse Cat");
        }

        public override Card OnCardUsing(Card card, Participant actor, ICollection<Participant> participants, IList<Participant> turnOrder, Deck deck)
        {
            base.OnCardUsing(card, actor, participants, turnOrder, deck);

            // Only react to an ExplodingKitten.
            // If it's an ExplodingKitten card, reset the actor's isAlive to true
            // Re-add the ExplodingKitten card to a random place in the deck
            // Return this card if you defused an ExplodingKitten card

            // Return null if you did not find any ExplodingKitten card
            if (card is ExplodingKitten)
            {
                actor.IsAlive = true;
                deck.AddCard(card, random.Next(0, deck.CardPile.Count()));
                deck.AddDiscardedCard(this);
                Console.WriteLine("You do have a defuse card! Awesome!!! Life to fight for another... round?");
                return this;
            }
            else return null;
        }
    }
}
