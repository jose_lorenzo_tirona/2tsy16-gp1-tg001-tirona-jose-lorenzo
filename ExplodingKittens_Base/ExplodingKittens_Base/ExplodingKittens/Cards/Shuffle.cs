﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class Shuffle : Card
    {
        public Shuffle()
        {
            CanUseManually = true;
        }

        public override void DisplayCard()
        {
            Console.WriteLine("Shuffle Cat");
        }

        public override void Use(Participant actor, ICollection<Participant> participants, IList<Participant> turnOrder, Deck deck)
        {
            base.Use(actor, participants, turnOrder, deck);
            Console.WriteLine(actor.Name + " used Shuffle on the deck!");

            // Call Shuffle on the deck
            deck.Shuffle();
            
        }
    }
}
