﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class Program
    {
        static void Main(string[] args)
        {
            Deck deck = new Deck();

            string name = "";
            Console.WriteLine("WELCOME TO EXPLODING KITTENS");
            Console.WriteLine("\n==========KABOOM==========\n");
            Console.WriteLine("Today you will be playing against our e-sports team: SOIL WHACKERS");
            Console.Write("What is your name brave soul? ");
            name = Console.ReadLine();

            Player player = new Player(name);

            List<Participant> participants = new List<Participant>(CreateParticipants());
            participants.Add(player);

            List<Participant> turnOrder = new List<Participant>(participants);

            deck.DealCards(participants);
            deck.CompleteSetup();
            foreach (Card card in deck.CardPile)
                card.DisplayCard();

            Console.WriteLine("Let the games begin!");
            Console.ReadKey();
            Console.Clear();
            while (participants.Count > 1)
            {
                // Get the first participant in list
                Participant currentParticipant = turnOrder.First();
                turnOrder.Remove(currentParticipant);

                // Requeue the participant to the back of the list
                turnOrder.Insert(turnOrder.Count, currentParticipant);

                // While the current participant still turns left
                while (currentParticipant.NumTurns > 0)
                {
                    // Evaluate current participant's turn
                    currentParticipant.EvaluateTurn(participants, turnOrder, deck);
                    currentParticipant.NumTurns--;
                }

                // Check if the participant is dead
                if (!currentParticipant.IsAlive)
                {
                    // If dead, remove the participant from the List of participants and the turn order
                    participants.Remove(currentParticipant);
                    while (turnOrder.Contains(currentParticipant))
                    {
                        turnOrder.Remove(currentParticipant);
                    }
                }

                // Reset the current participant's turns to 1
                currentParticipant.NumTurns = 1;
                Console.WriteLine();
                Console.ReadKey();
                Console.Clear();
            }

            Console.WriteLine("{0} Wins the game!", participants[0].Name);

            Console.Read();

        }

        static IEnumerable<Participant> CreateParticipants()
        {
            AI ai1 = new AI("Loraine");
            AI ai2 = new AI("Cyril");
            AI ai3 = new AI("Marco");
            AI ai4 = new AI("Anj");

            List<AI> ais = new List<AI>();
            ais.Add(ai1);
            ais.Add(ai2);
            ais.Add(ai3);
            ais.Add(ai4);

            return ais;
        }
    }
}
