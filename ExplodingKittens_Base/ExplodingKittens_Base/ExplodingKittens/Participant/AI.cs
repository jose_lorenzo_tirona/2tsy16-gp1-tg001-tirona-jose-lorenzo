﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class AI : Participant
    {
        public AI (string name) : base (name)
        {

        }

        /// <summary>
        /// Play a random card
        /// </summary>
        public override void EvaluateTurn(ICollection<Participant> participants, IList<Participant> turnOrder, Deck deck)
        {
            // While the AI still has usable cards, do a 50-50 chance to use a card or end the turn
                // 50-50 chances to use a card or pass
                    // Upon using a card, get the card from PlayCard
                    // Remove it from the hand
                    // Use the card
                    // Add it to the discard pile of the deck
                    

            // End of turn, check if I need to draw a card
            
            // Always reset should draw to true, because after every turn you should draw          

            // Reset turn ended to false
            while(!TurnEnded)
            {
                int move;
                foreach(Card card in hand)
                {
                    if (card.CanUseManually) break;

                }

                move = random.Next(1);
                if (move == 0)
                {
                    PlayCard();
                    hand.Remove(PlayCard());
                    PlayCard().Use(this, participants, turnOrder, deck);
                    deck.AddDiscardedCard(PlayCard());
                }
                else
                {
                    if (ShouldDraw)
                        DrawCard(participants, turnOrder, deck);
                    TurnEnded = true;
                }
            }
            this.ShouldDraw = true;
            this.TurnEnded = false;
        }

        public override Card PlayCard()
        {
            int card;
            while (true)
            {
                card = random.Next(hand.Count);
                if (hand[card].CanUseManually) break;
            }
            return hand[card];
        }

        /// <summary>
        /// Selects a random target that isn't the AI
        /// </summary>
        /// <param name="participants"></param>
        /// <returns></returns>
        public override Participant SelectTarget(ICollection<Participant> participants)
        {
            // Do not select yourself
            throw new NotImplementedException();
        }
    }
}
