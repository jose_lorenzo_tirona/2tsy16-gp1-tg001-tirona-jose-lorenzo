﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    class Player : Participant
    {
        public Player(string name) : base(name)
        {

        }

        public override void EvaluateTurn(ICollection<Participant> participants, IList<Participant> turnOrder, Deck deck)
        {
            // While the Player still has usable cards, ask if the player wants to use a card
            // Upon using a card, get the card from PlayCard
            // Remove it from the hand
            // Use the card
            // Add it to the discard pile of the deck

            // End of turn, check if I need to draw a card

            // Always reset should draw to true, because after every turn you should draw

            // Reset turn ended to false
            this.DisplayHand();
            while (!TurnEnded)
            {
                Console.WriteLine("Do you want to play a card? (y / n)");
                char move = Convert.ToChar(Console.ReadLine());

                if (move == 'y')
                {
                    PlayCard();
                    hand.Remove(PlayCard());
                    PlayCard().Use(this, participants, turnOrder, deck);
                    deck.AddDiscardedCard(PlayCard());
                }
                else if (move == 'n')
                {
                    if (ShouldDraw)
                        DrawCard(participants, turnOrder, deck);
                    TurnEnded = true;
                }
            }
            this.ShouldDraw = true;
            this.TurnEnded = false;
        }

        public override Card PlayCard()
        {
            DisplayHand();
            int move = Convert.ToInt32(Console.ReadLine());
            return hand[move];
        }
        /// <summary>
        /// Give the user a selection of targets
        /// </summary>
        /// <param name="participants"></param>
        /// <returns></returns>
        public override Participant SelectTarget(ICollection<Participant> participants)
        {
            // Do not select yourself
            // Print all participants excluding yourself
            int x = 0;

            Console.WriteLine("Choose a target: ");
            foreach (Participant participant in participants)
            {
                if(participant != this)
                    Console.WriteLine(x + "). " + participant.Name);
            }
            int move = Convert.ToInt32(Console.ReadLine());
            return participants.ElementAt(move);
        }
    }
}
