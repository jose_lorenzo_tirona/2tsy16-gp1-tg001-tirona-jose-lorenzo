﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplodingKittens
{
    abstract class Participant
    {
        protected Random random = new Random();
        protected List<Card> hand = new List<Card>();
        
        public ICollection<Card> Hand { get { return hand; } }

        public bool ShouldDraw { get; set; }
        public bool TurnEnded { get; set; }
        public bool IsAlive { get; set; }
        public int NumTurns { get; set; }

        public string Name { get; private set; }

        public Participant(string name)
        {
            Name = name;
            ShouldDraw = true;
            TurnEnded = false;
            IsAlive = true;
            NumTurns = 1;
        }

        public void DisplayHand()
        {
            Console.WriteLine("======= CARDS =======");
            foreach(Card card in hand)
            {
                Console.WriteLine(hand.IndexOf(card) + "). ");
                card.DisplayCard();
            }
        }

        public abstract void EvaluateTurn(ICollection<Participant> participants, IList<Participant> turnOrder, Deck deck);
        public abstract Participant SelectTarget(ICollection<Participant> participants);
        public abstract Card PlayCard();

        public void DrawCard(ICollection<Participant> participants, IList<Participant> turnOrder, Deck deck)
        {
            // Automatically use the card if it's an ExplodingKitten card
                // Remove the explodding kitten card from your hand
            deck.DrawCard();
            if (PlayCard() is ExplodingKitten)
            {
                PlayCard().Use(this, participants, turnOrder, deck);
                this.hand.Remove(PlayCard());
            }
        }
    }
}
