﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_4_16
{
    public class Skill
    {
        public string Name;
        public int Power;
        public int Cost;
        public Status Status { get; set; }

    }
}
