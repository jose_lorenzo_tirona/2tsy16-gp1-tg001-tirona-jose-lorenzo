﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_4_16
{
    public class Equip
    {
        public string Name;
        public int MaxPower;
        public int MinPower;
        public Status Status { get; set; }

        public Equip(string name, int max, int min, Status status)
        {
            Name = name;
            MaxPower = max;
            MinPower = min;
            Status = status;
        }
    }
}
