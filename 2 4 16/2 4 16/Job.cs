﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_4_16
{
    public class Job
    {
        public string Name;
        public Skill Skill { get; set; }

        public Job(string name, Skill skill)
        {
            Name = name;
            Skill = skill;
        }
    }
}
