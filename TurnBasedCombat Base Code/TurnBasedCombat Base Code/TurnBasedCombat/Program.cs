﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedCombat
{
    class Program
    {
        // You don't need to do anything here
        static void Main(string[] args)
        {
            
            // Prepare team
            Team player = CreatePlayerTeam();
            Team enemy = CreateEnemyTeam();
            
            // Initiate combat
            Combat combat = new Combat();
            combat.Player = player;
            combat.Enemy = enemy;
            combat.Initiate(); // This will not finish until a winner is determined

            // Combat has ended at this point
            Console.ReadKey();
        }

        /// <summary>
        /// Create a Team of PlayerUnits with all their Actions. Use the functions already available in the supplied classes
        /// To create an Action, use the constructor and pass in the Unit who owns that Action
        /// Remember that you need to instantiate an action for each unit. Do not reuse the same instance for all units.
        /// </summary>
        /// <returns>Player Team</returns>
        static Team CreatePlayerTeam()
        {
            Unit warrior = new PlayerUnit()
            {
                Name = "Bob",
                Job = Job.Warrior,


            };

            Unit assassin = new PlayerUnit()
            {
                Name = "Bill",
                Job = Job.Assassin,

            };

            Unit mage = new PlayerUnit()
            {
                Name = "Ben",
                Job = Job.Mage,

            };



        }

        /// <summary>
        /// Create a Team of EnemyUnits with all their Actions. Use the functions already available in the supplied classes
        /// To create an Action, use the constructor and pass in the Unit who owns that Action.
        /// Remember that you need to instantiate an action for each unit. Do not reuse the same instance for all units.
        /// </summary>
        /// <returns>Enemy Team</returns>
        static Team CreateEnemyTeam()
        {
            throw new NotImplementedException();
        }
    }
}
