﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedCombat
{
    public class Combat
    {
        public Team Player { get; set; }
        public Team Enemy { get; set; }
        public Team Winner { get; private set; }

        private List<Unit> turnOrder = new List<Unit>();
        public IEnumerable<Unit> TurnOrder { get { return turnOrder; } }

        public void Initiate()
        {
            // Set turn order. DON'T CHANGE
            EvaluateTurnOrder();

            // Start battle. Call ProcessTurn. (You may choose to loop this or call the function recursively).
            ProcessTurn(); // Commented intentionally. Change this line depending if you want a loop or a recursive call

            // Should not continue here until a winner is determined. DO NOT DELETE THIS LINE.
            if (Winner == null) throw new Exception("Cannot end Combat if the winner is not determined yet.");

            // Battle result. DON'T CHANGE
            Console.Clear();
            Console.WriteLine(Winner.Name + " wins!\n");
            Winner.DisplayTeam();
        }

        /// <summary>
        /// Create a turn order where the Unit with the highest AGI goes first
        /// 1. Clear the turnOrder list (to prevent unexpected behavior)
        /// 2. Add all player units to the turnOder list
        /// 3. Add all enemy units to the turnOrder list
        /// 4. Sort the turnOrder list in ascending order based on AGI
        /// </summary>
        private void EvaluateTurnOrder()
        {
            turnOrder.Clear();
            foreach(Unit unit in Player.Members)
            {
                turnOrder.Add(unit);
            }
            foreach (Unit unit in Enemy.Members)
            {
                turnOrder.Add(unit);
            }
            var sortedOrder = turnOrder.OrderBy(x => x.Stats.Agility).ToList();
        }

        /// <summary>
        /// Process ONE turn. This turn should be for the unit at the first unit in the current TurnOrder
        /// 1. Check if there's already a winner. If there is, set the Winner property and exit out of the function IMMEDIATELY.
        /// 2. Call PrintCurrentTurn
        /// 3. Get the first unit in current TurnOrder. It will be this unit's turn.
        /// 4. Execute the turn of the current unit. Call Unit.EvaluateTurn then pass in "this" combat as parameter.
        /// 5. Check for dead unis and remove them from the turn order. Loop through the list and CAREFULLY remove the unit. Remember that you cannot remove an element in a list while it's still being enumerated (foreach). Instead, use a separate list to store the dead units so that you are enumerating a different list, preventing the exception.
        /// 6. Remove the current unit from the turn order and add it to the end of the turn order list. This will requeue the unit so he can act again later.
        /// 7. Clear the console
        /// </summary>
        private void ProcessTurn()
        {
            // 1. Check if there's already a winner. If there is, exit out of the function (this ends the game)
            while (Winner == null)
            {
                // 2. Display turn. Free code.
                PrintCurrentTurn();

                // 3. Get the current unit
                Unit current = turnOrder.First();
                // 4. Evaluate the turn of the current unit
                current.EvaluateTurn(this);
                turnOrder.Remove(current);
                // 5. Remove dead units from the turn order.
                foreach(Unit unit in turnOrder)
                {
                    if (!unit.Alive) turnOrder.Remove(unit);
                }
                // 6. Requeue the unit
                if(current.Alive) turnOrder.Add(current);
                // 7. Clear console for next turn. Free code.
                Console.ReadKey();
                Console.Clear();

                // Recursive call here if you chose a recursive call instead of a loop. Delete this comment if you chose a loop instead.

                
            }
        }

        /// <summary>
        /// Free code. Called by ProcessTurn to display the current turn
        /// </summary>
        private void PrintCurrentTurn()
        {
            // Display all units
            PrintTeams();
            Console.WriteLine();
            PrintTurnOrder();

            Unit current = turnOrder.First();
            Console.WriteLine();
            Console.WriteLine("Current Turn: [" + current.Team.Name + "] " + current.Name);
            Console.ReadKey();
            Console.Clear();
        }

        /// <summary>
        /// Print the turn order list. It should represent the "current" turn order. The unit who should act should be on top of the list.
        /// </summary>
        private void PrintTurnOrder()
        {
            Console.WriteLine("========= TURN ORDER =========");
            foreach(Unit unit in turnOrder)
            {
                Console.WriteLine();
                Console.WriteLine("#" + (turnOrder.IndexOf(unit) + 1) + " [" + unit.Team.Name + "] " + unit.Name);
            }
            Console.WriteLine("========= TURN ORDER =========");

        }


        /// <summary>
        /// Free code. Called by PrintCurrentTurn
        /// </summary>
        private void PrintTeams()
        {
            Player.DisplayTeam();
            Console.WriteLine();
            Enemy.DisplayTeam();
        }
    }
}
