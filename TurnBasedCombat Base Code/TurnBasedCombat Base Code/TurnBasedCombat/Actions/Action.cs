﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedCombat
{
    public abstract class Action
    {
        public string Name { get; set; }
        public int MpCost { get; set; }
        public Unit Actor { get; private set; }

        /// <summary>
        /// Returns true if the Actor has enough MP to use this Action
        /// </summary>
        public bool CanUse
        {
            get
            {
                if (Actor.CurrentMp >= MpCost) return true;
                else return false;
            }
        }

        /// <summary>
        /// Automatically adds this Action to the Actor. Nothing to do here
        /// </summary>
        /// <param name="actor"></param>
        public Action(Unit actor)
        {
            Actor = actor;
            Actor.Actions.Add(this);
        }

        /// <summary>
        /// Executes an action if there's enough MP
        /// Nothing to do here
        /// </summary>
        /// <param name="combat"></param>
        public virtual void Execute(Combat combat)
        {
            // Makes sure that this is only called if actor has enough MP. DON'T DELETE THIS.
            if (!CanUse) throw new Exception("Cannot execute an Action if Actor doesn't have enough MP");

            // Actor has enough MP, reduce actor's MP by MP cost
            Actor.CurrentMp -= MpCost;
        }
    }
}
