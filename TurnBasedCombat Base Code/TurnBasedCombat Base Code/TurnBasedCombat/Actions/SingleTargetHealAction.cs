﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedCombat
{
    public class SingleTargetHealAction : Action
    {
        public SingleTargetHealAction(Unit actor)
            : base(actor)
        {
        }

        public float HealPercent { get; set; }

        /// <summary>
        /// Heal an ally (same team) with the lowest HP based on set HealPercent
        /// Use Combat.TurnOrder to get ALL units
        /// </summary>
        /// <param name="combat"></param>
        /// <returns></returns>
        public override void Execute(Combat combat)
        {
            // Deducts MP by MpCost. Makes sure that the Action can be executed. Crashes if you call this while not having enough MP. DON'T DELETE THIS.
            base.Execute(combat);

            //target hp += max hp*0.3f
            throw new NotImplementedException();
        }
    }
}
