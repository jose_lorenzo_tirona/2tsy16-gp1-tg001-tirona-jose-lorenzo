﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedCombat
{
    public class Team
    {
        public string Name { get; set; }

        private List<Unit> members = new List<Unit>();
        public IEnumerable<Unit> Members { get { return members; } }

        /// <summary>
        /// Returns true if all Units in this team is dead (Unit.Alive returns false). False, otherwise
        /// </summary>
        public bool Wiped
        {
            get
            {
                foreach(Unit unit in members)
                {
                    if (unit.Alive) return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Adds a Unit to this Team
        /// Make sure to set Unit.Team to this Team before/after adding the Unit to the members list
        /// </summary>
        /// <param name="unit"></param>
        public void AddMember(Unit unit)
        {
            // Makes sure that the Unit wasn't previously added to any team. DON'T DELETE THIS
            if (unit.Team != null) throw new Exception("Unit is already a part of a team");

            // Add the unit to the list
            unit.Team = null;
            members.Add(unit);
        }

        /// <summary>
        /// Removes a Unit from this Team
        /// Make sure to set Unit.Team to null before/after removing the Unit from the members list
        /// </summary>
        /// <param name="unit"></param>
        public void RemoveMember(Unit unit)
        {
            // Make sure that this Unit is not part of other teat. DO NOT DELETE
            if (!members.Contains(unit)) throw new Exception("Cannot remove a Unit that isn't part of this team.");

            unit.Team = null;
            members.Remove(unit);
        }

        /// <summary>
        /// Print the team name and all of its members
        /// Print the normalized value (percent of HP) of each member. Check the sample build.
        /// </summary>
        public void DisplayTeam()
        {
            Console.WriteLine("Team: " + Name + "/n=====================================");
            foreach(Unit unit in members)
            {
                Console.WriteLine("{0}/t[{0}]", unit.Name, unit.NormalizedHp);
            }
            Console.WriteLine();
        }
    }
}
