﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedCombat
{
    public enum Job { Warrior, Assassin, Mage }

    public abstract class Unit
    {
        // You can use these constants so that you don't have to declare them yourself
        private static readonly float DAMAGE_VARIANCE = 0.2f;
        private static readonly float CLASS_ADVANTAGE_DAMAGE_MULTIPLIER = 1.5f;

        public string Name { get; set; }
        public Job Job { get; set; }
        public Stats Stats { get; set; }
        public Team Team { get; set; }

        private List<Action> _actions = new List<Action>();
        public IList<Action> Actions { get { return _actions; } }

        private int _currentHp;
        public int CurrentHp
        {
            get { return _currentHp; }
            set
            {
                // Clamp value of HP to 0 and Stats.Hp
                _currentHp = value;
                if (_currentHp < 0) _currentHp = 0;
                if (_currentHp > Stats.Hp) _currentHp = Stats.Hp;
            }
        }

        private int _currentMp;
        public int CurrentMp
        {
            get { return _currentMp; }
            set
            {
                // Clamp value of MP to 0 and Stats.Mp
                _currentMp = value;
                if (_currentMp < 0) _currentMp = 0;
                if (_currentMp > Stats.Mp) _currentMp = Stats.Mp;
            }
        }

        /// <summary>
        /// Tells if the unit's HP is still above zero. A dead unit cannot take part in Combat
        /// </summary>
        public bool Alive { get { return CurrentHp > 0; } }

        /// <summary>
        /// A value ranging from 0.0 to 1.0 (percent of HP)
        /// If the Unit's HP is 30/100, then this function should return 0.3f
        /// </summary>
        public float NormalizedHp
        {
            get
            {
                float normalizedHP = _currentHp / Stats.Hp;
                return normalizedHP;
            }
        }

        /// <summary>
        /// Set current HP and MP to full (based on Stats.Hp and Stats.Mp)
        /// </summary>
        public void RestoreHpMp()
        {
            _currentHp = Stats.Hp;
            _currentMp = Stats.Mp;
        }

        /// <summary>
        /// Compute damage randomly based on variance. Where variance is 20% of Power
        /// If your Power is 100, random damage should range from 100-119. 20% of 100 is 20, so there will be 20 possible values for damage.
        /// </summary>
        /// <returns></returns>
        public int RandomizeDamage()
        {
            int randomized = Stats.Power + RandomHelper.Range(1, Convert.ToInt32(Stats.Power * DAMAGE_VARIANCE));
            return randomized;
        }

        /// <summary>
        /// Determine whether to apply class advantage bonus or not (150%).
        /// For example, Warrior beats Assassin so this function should return 1.5f.
        /// If bonus is not applicable, simply return 1.0f
        /// Use Unit.Job to check for class type
        /// </summary>
        /// <param name="target"></param>
        /// <returns>1.5f or 1.0f</returns>
        public float EvaluateDamageMultiplier(Unit target)
        {
            if (this.Job == Job.Warrior)
                if (target.Job == Job.Assassin) return CLASS_ADVANTAGE_DAMAGE_MULTIPLIER;
            if (this.Job == Job.Assassin)
                if (target.Job == Job.Mage) return CLASS_ADVANTAGE_DAMAGE_MULTIPLIER;
            if (this.Job == Job.Mage)
                if (target.Job == Job.Warrior) return CLASS_ADVANTAGE_DAMAGE_MULTIPLIER;
            return 1.0f;
        }

        /// <summary>
        /// This function determines which action to take per turn. This needs to be implemented in PlayerUnit and EnemyUnit
        /// </summary>
        /// <param name="combat"></param>
        public abstract void EvaluateTurn(Combat combat);

        /// <summary>
        /// Executes an action. Can only execute an Action that is owned by this Unit
        /// Nothing to do here
        /// </summary>
        /// <param name="action"></param>
        /// <param name="combat"></param>
        public void ExecuteAction(Action action, Combat combat)
        {
            // Make sure that this Unit owns this Action
            if (!Actions.Contains(action)) throw new Exception("Cannot execute an Action that isn't owned by this Unit");

            // Execute the action
            action.Execute(combat);
        }

        /// <summary>
        /// Display the Unit's information (Name, Class, HP/MP, Stats)
        /// </summary>
        public void DisplayUnit()
        {
            Console.WriteLine("Name: " + Name);
            Console.WriteLine("Class: " + Job);
            Stats.DisplayStats();
        }
    }
}
