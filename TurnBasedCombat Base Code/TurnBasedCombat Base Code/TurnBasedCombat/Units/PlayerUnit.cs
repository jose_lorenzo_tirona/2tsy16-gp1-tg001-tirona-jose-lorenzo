﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedCombat
{
    public class PlayerUnit : Unit
    {
        /// <summary>
        /// Evaluate the action to be used for the PlayerUnit
        /// 1. Display the Unit
        /// 2. Create a list of valid actions (enough MP)
        /// 3. Print the list to display the actions the user can take
        /// 4. Get user input for action selection. Make sure that the user is inputting valid commands
        /// 5. Clear the console
        /// 6. Execute the action (Action.Execute). Remember to pass in the combat parameter to Action.Execute.
        /// </summary>
        /// <param name="combat"></param>
        public override void EvaluateTurn(Combat combat)
        {
            DisplayUnit();
            List<Action> validActions = new List<Action>();

            foreach(Action action in Actions)
            {
                if (action.CanUse) validActions.Add(action);
            }
            foreach(Action action in validActions)
            {
                Console.WriteLine("[{0}] {0}", validActions.IndexOf(action), action.Name);
            }
            int x = Convert.ToInt32(Console.ReadLine());
            Console.Clear();

            validActions[x].Execute(combat);
        }
    }
}
