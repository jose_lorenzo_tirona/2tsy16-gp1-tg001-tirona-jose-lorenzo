﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedCombat
{
    public class EnemyUnit : Unit
    {
        /// <summary>
        /// Randomize an action to be used by the EnemyUnit
        /// 1. Create a list of valid actions (enough MP)
        /// 2. Get a random Action based on the valid action list
        /// 3. Execute the action (Action.Execute). Remember to pass in the combat parameter to Action.Execute.
        /// </summary>
        /// <param name="combat"></param>
        public override void EvaluateTurn(Combat combat)
        {
            List<Action> validActions = new List<Action>();
            foreach (Action action in Actions)
            {
                if (action.CanUse) validActions.Add(action);
            }
            int x = RandomHelper.Range(validActions.Count);
            validActions[x].Execute(combat);
        }
    }
}
