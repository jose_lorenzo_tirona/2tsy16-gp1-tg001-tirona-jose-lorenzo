﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tirona
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] items = { "Health Potion", "Mana Potion", "Yggdrasil Leaf", "Dreadwyrm Staff", "Holy Robe" };
            Console.WriteLine("Wot do ya need, lad?");
            string input = Console.ReadLine();
            SearchArray(items, input);
            int input2 = Convert.ToInt32(Console.ReadLine());
            SearchItem(items, input2);
        }

        static void SearchArray(string[] items, string itemToSearch)
        {
            for (int i = 0; i < items.Length; i++)
            {
                if (items[i] == itemToSearch)
                {
                    Console.WriteLine("Item #{0} Found", i + 1);
                    break;
                }
                else if (i == items.Length - 1)
                    Console.WriteLine("Not Found");
            }
        }
        static void SearchItem(string[] items, int itemNumber)
        {
            for (int i = 0; i < items.Length; i++)
            {
                if (i == itemNumber)
                {
                    Console.WriteLine("{0}", items[i - 1]);
                    break;
                }
                else if (i == items.Length - 1)
                    Console.WriteLine("Not Found");

            }
        }
    }
}
