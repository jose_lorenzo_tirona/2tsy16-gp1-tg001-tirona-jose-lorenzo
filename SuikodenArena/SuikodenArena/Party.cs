﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuikodenArena
{
    public class Party
    {
        /// <summary>
        /// Use this to give the party some kind of identity
        /// </summary>
        public string Name { get; set; }

        // DO NOT CHANGE THESE 2 LINES
        private List<Unit> units = new List<Unit>();
        public IEnumerable<Unit> Units { get { return units; } }

        /// <summary>
        /// If there's no more units left (units list count is 0), return true. False, otherwise
        /// </summary>
        public bool Wiped
        {
            get
            {
                if (units.Count > 0) return false;
                else return true;
            }
        }

        /// <summary>
        /// Add the unit to the 'units' list
        /// </summary>
        /// <param name="unit"></param>
        public void AddUnit(Unit unit)
        {
            units.Add(unit);
        }

        /// <summary>
        /// Get the next unit in queue. This unit is the index 0 of the 'units' list. 
        /// Remove the returned unit from the list.
        /// </summary>
        /// <returns>If the list is empty, return null. Returns the unit at index 0 if the list has at least one unit.</returns>
        public Unit NextUnit()
        {
            if (!units[0].Alive) units.RemoveAt(0);
            if (!Wiped) return units[0];
            else return null;
            
        }

        /// <summary>
        /// Print team name and all the units in the party (Console.WriteLine)
        /// </summary>
        public void DisplayParty()
        {
            
            Console.WriteLine("\n\n{0}\n===============================================\n",Name);
            foreach (Unit unit in units) unit.DisplayStats();
        }
    }
}
