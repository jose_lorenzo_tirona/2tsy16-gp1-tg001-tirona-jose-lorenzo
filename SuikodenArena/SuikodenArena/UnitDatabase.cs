﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuikodenArena
{
    public class UnitDatabase
    {
        private static readonly Random rand = new Random();
        public static Unit GetHero(int id)
        {
            int hp = rand.Next(90, 100), pow = rand.Next(15, 25), def = rand.Next(5, 10);
            if (id == 0) return new Unit("Phosky", hp, pow, def);
            else if (id == 1) return new Unit("HeeSoo", hp, pow, def);
            else if (id == 2) return new Unit("DK", hp, pow, def);
            else if (id == 3) return new Unit("Storror", hp, pow, def);
            else if (id == 4) return new Unit("Farang", hp, pow, def);
            else if (id == 5) return new Unit("Shobu", hp, pow, def);
            else if (id == 6) return new Unit("Pasha", hp, pow, def);
            else if (id == 7) return new Unit("Zyulev", hp, pow, def);
            else if (id == 8) return new Unit("Steel", hp, pow, def);
            else if (id == 9) return new Unit("Peveril", hp, pow, def);

            else return null;
        }
    }
}
