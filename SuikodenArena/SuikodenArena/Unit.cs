﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuikodenArena
{
    /// <summary>
    /// Possible actions a unit can execute. Here's an example on how to use this
    /// Some examples:
    /// Ex#1 Action action = ActionType.Attack
    /// Ex#2 if (action == ActionType.Attack)
    /// Ex#3 return ActionType.Attack
    /// </summary>
    public enum ActionType
    {
        None,
        Attack,
        Defend,
        WildAttack
    }

    public class Unit
    {
        public Unit(string name, int hp, int pow, int def)
        {
            Name = name;
            MaxHp = hp;
            Power = pow;
            Defense = def;
            CurrentHp = hp;
        }

        /// <summary>
        /// Use this object to randomize value. This will prevent getting the same values.
        /// To use, call this function: random.Next(int maxvalue) or random.Next(int minValue, int maxValue);
        /// </summary>
        private static readonly Random random = new Random();

        public string Name { get; set; }
        public int Power { get; set; }
        public int Defense { get; set; }
        public int MaxHp { get; set; }

        /// <summary>
        /// Return true if CurrentHp is greater than 0. False, otherwise.
        /// </summary>
        public bool Alive
        {
            get
            {
                if (CurrentHp > 0) return true;
                else
                {
                    Console.WriteLine("\nOh no! \n\n{0} died.\n\n", Name);
                    Console.ReadKey(); Console.Clear();
                    return false;
                }
            }
        }

        /// <summary>
        /// The variable being set by SetUserAction and SetRandomAction (please refer to these functions). DON'T MODIFY THIS LINE
        /// </summary>
        public ActionType Action { get; private set; }

        /// <summary>
        /// Write an appropriate algorithm for clamping the HP between 0 and MaxHp
        /// For getter, simply return 'currentHp'
        /// For setter, update 'currentHp' value. Make sure that 'currentHp' is set to a value between 0 and MaxHp
        /// </summary>
        private int currentHp;
        public int CurrentHp
        {
            // Don't change this
            get { return currentHp; }

            // Implement this
            set
            {
                if (currentHp < 0) currentHp = 0;
                currentHp = value;
            }
        }

        /// <summary>
        /// Randomize damage based on Power.
        /// Minimum damage = Your Power
        /// Maximum damage = 120% of your power (eg. if Power = 100, then max damage = 120)
        /// </summary>
        /// <returns></returns>
        public int RandomizeDamage(int sitch, Unit target)
        {
            // 1 = 50%
            // 2 = 100%
            // 3 = 200%
            int maxDam = Convert.ToInt32(Power+Power*0.2);
            int damage = random.Next(Power, maxDam);
            if (sitch == 1) damage = Convert.ToInt32(damage * 0.5) - target.Defense;
            else if (sitch == 2) damage -= target.Defense;
            else if (sitch == 3) damage = (damage * 2) - target.Defense;

            if (damage < 1)
                damage = 1;

            return damage;
        }

        /// <summary>
        /// Evaluate this unit's action and the target's action. Based on the action, deal damage with appropriate damage modifier (if applicable). 
        /// This function only tries to damage the target from the perspective of this unit. Target's damage will be evaluated on the target's side.
        /// Refer to the matchup table in the spec for damage modifiers.
        /// TIP: Read this.Action and target.Action and compare these two.
        /// </summary>
        /// <param name="target"></param>
        public void EvaluateAction(Unit user, Unit ai)
        {
            SetUserAction();
            if (Action == ActionType.Attack)
            {
                Console.WriteLine("\n{0} used consecutive normal punches.\n", user.Name);
                SetRandomAction();
                if (Action == ActionType.Attack)
                {
                    Console.WriteLine("\n{0} used consecutive normal punches.\n", ai.Name);
                    user.currentHp -= ai.RandomizeDamage(2,user);
                    Console.WriteLine("\nHIT! {0} lost {1} HP!\n", user.Name, ai.RandomizeDamage(2,user));
                    ai.currentHp -= user.RandomizeDamage(2,ai);
                    Console.WriteLine("\nHIT! {0} lost {1} HP!\n", ai.Name, user.RandomizeDamage(2,ai));
                }
                else if (Action == ActionType.Defend)
                {
                    Console.WriteLine("\n{0} defended.", ai.Name);
                    ai.CurrentHp -= user.RandomizeDamage(1,ai);
                    Console.WriteLine("\nIt was sorta effective. {0} lost {1} HP.\n", ai.Name, user.RandomizeDamage(1,ai));
                }
                else if (Action == ActionType.WildAttack)
                {
                    Console.WriteLine("\n{0} used Splash.\n", ai.Name);
                    user.currentHp -= ai.RandomizeDamage(3,user);
                    Console.WriteLine("\nCOUNTER SPLASHED! Get rekt. {0} lost {1} HP! It was super effective.\n", user.Name, ai.RandomizeDamage(3,user));
                    ai.currentHp -= user.RandomizeDamage(2,ai);
                    Console.WriteLine("\nHIT! {0} lost {1} HP!\n", ai.Name, user.RandomizeDamage(2,ai));
                }
                Action = ActionType.None;
            }
            else if (Action == ActionType.Defend)
            {
                Console.WriteLine("\n{0} defended.\n", user.Name);
                SetRandomAction();
                if (Action == ActionType.Attack)
                {
                    Console.WriteLine("{0} used consecutive normal punches.", ai.Name);
                    user.CurrentHp -= ai.RandomizeDamage(1,user);
                    Console.WriteLine("\nIt was sorta effective. {0} lost {1} HP.\n", user.Name, ai.RandomizeDamage(1,user));
                }
                else if (Action == ActionType.Defend)
                    Console.WriteLine("{0} also defended. \n------*awkward silence*------\n", ai.Name);
                else if (Action == ActionType.WildAttack)
                {
                    Console.WriteLine("{0} used SPLASH.", ai.Name);
                    ai.CurrentHp -= user.RandomizeDamage(3,ai);
                    Console.WriteLine("\nCOUNTER SPLASHED! Get rekt. {0} lost {1} HP! It was super effective.\n", ai.Name, user.RandomizeDamage(3,ai));
                }
                Action = ActionType.None;
            }
            else if (Action == ActionType.WildAttack)
            {
                Console.WriteLine("\n{0} used Splash.\n", user.Name);
                SetRandomAction();
                if (Action == ActionType.Attack)
                {
                    Console.WriteLine("\n{0} used consecutive normal punches.\n", ai.Name);
                    user.CurrentHp -= ai.RandomizeDamage(2,user);
                    Console.WriteLine("\nHIT! {0} lost {1} HP.\n", user.Name, ai.RandomizeDamage(2,user));
                    ai.CurrentHp -= user.RandomizeDamage(3,ai);
                    Console.WriteLine("\nHIT! {0} lost {1} HP.\n", ai.Name, user.RandomizeDamage(3,ai));
                }
                else if (Action == ActionType.Defend)
                {
                    Console.WriteLine("\n{0} defended.", ai.Name);
                    user.CurrentHp -= ai.RandomizeDamage(3,user);
                    Console.WriteLine("\nCOUNTER SPLASHED! Get rekt. {0} lost {1} HP! It was super effective.\n", user.Name, ai.RandomizeDamage(3,user));
                }
                else if (Action == ActionType.WildAttack)
                {
                    Console.WriteLine("\n{0} used Splash.\n", ai.Name);
                    ai.CurrentHp -= user.RandomizeDamage(3,user);
                    Console.WriteLine("\nHIT! {0} lost {1} HP! It was super effective!\n", ai.Name, user.RandomizeDamage(3,user));
                    user.CurrentHp -= ai.RandomizeDamage(3,user);
                    Console.WriteLine("\nHIT! {0} lost {1} HP! It was super effective!\n", user.Name, ai.RandomizeDamage(3,user));
                }
                Action = ActionType.None; 
            }
            
        }

        /// <summary>
        /// Call this to set action based from user input.
        /// Ask the user for input and return an enum value.
        /// After determining the action, set the Action variable based on evaluated value.
        /// TIP: Ask the player to input a number. Where a number is tied to one action
        ///     1 = ActionType.Attack
        ///     2 = ActionType.Defend
        ///     3 = ActionType.WildAttack
        /// </summary>
        public void SetUserAction()
        {
            Console.WriteLine("Choose an action:\n\t[1]\tAttack\n\t[2]\tDefend\n\t[3]\tWild Attack\n");
            string move = Console.ReadLine();
            int value;
            if (int.TryParse(move, out value))
            {
                if (value == 1) Action = ActionType.Attack;
                else if (value == 2) Action = ActionType.Defend;
                else if (value == 3) Action = ActionType.WildAttack;
            }
        }

        /// <summary>
        /// Call this to get action from AI (randomized)
        /// You can randomize 3 numbers where each number is tied to one action
        ///     1 = ActionType.Attack
        ///     2 = ActionType.Defend
        ///     3 = ActionType.WildAttack
        /// </summary>
        /// <returns>A randomized action</returns>
        public void SetRandomAction()
        {
            int move = random.Next(1, 3);
            if (move == 1) Action = ActionType.Attack;
            else if (move == 2) Action = ActionType.Defend;
            else if (move == 3) Action = ActionType.WildAttack;
        }

        /// <summary>
        /// Display the name, HP, and stats of this unit (Console.WriteLine)
        /// </summary>
        public void DisplayStats()
        {
            Console.WriteLine("{0}\t\tHP: {1}/{2}\tPOW: {3}\tDEF: {4}", Name, CurrentHp, MaxHp, Power, Defense);
        }
    }
}
