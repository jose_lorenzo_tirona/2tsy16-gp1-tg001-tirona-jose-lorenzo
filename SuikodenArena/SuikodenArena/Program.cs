﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuikodenArena
{
    class Program
    {
        public static readonly Random rand = new Random();
        static void Main(string[] args)
        {
            List<int> available = new List<int>();
            for (int i = 0; i < 10; i++) available.Add(i);
            
            // 1. Initialize the 2 parties
            Party user = new Party();
            user.Name = "GUP";
            for(int x=0; x<5; x++)
            {
                int i = rand.Next(available.Count);
                user.AddUnit(UnitDatabase.GetHero(available[i]));
                available.RemoveAt(i);
            }
            Party ai = new Party();
            ai.Name = "Krap Invaders";
            foreach(int num in available)
                ai.AddUnit(UnitDatabase.GetHero(3));
            
            // 2. Display the 2 parties (use Party.DisplayParty)
            user.DisplayParty();
            ai.DisplayParty();
            Console.ReadKey(); Console.Clear();
            // 3. Loop until at least one party is wiped
            while (!user.Wiped && !ai.Wiped)
            {
                // 3.1 Get the combatants (use Party.NextUnit())
                user.NextUnit();
                ai.NextUnit();
                // 3.2 Loop until at least one unit is dead
                while (ai.NextUnit() != null && user.NextUnit() != null)
                {
                    user.NextUnit().DisplayStats();
                    Console.WriteLine("{0} Left : {1} \nVS\n{2} Left: {3}", user.Name, user.Units.Count(), ai.Name, ai.Units.Count());
                    ai.NextUnit().DisplayStats();
                    user.NextUnit().EvaluateAction(user.NextUnit(), ai.NextUnit());
                    Console.ReadKey(); Console.Clear();
                    // 3.2.1 Display the units in battle (use Unit.DisplayStats())
                    // 3.2.1 Set action for both units (use Unit.SetUserAction and Unit.SetRandomAction)
                    // 3.2.2 Evaluate actions of the 2 units
                }
            }
            // 4. Evaluate the winner (may result in a win, lose, or draw)
            if (ai.Wiped) Console.WriteLine("Enemy team got wiped out!\nYou WIN! Congratulations.\n");
            else if (user.Wiped) Console.WriteLine("Your team got wiped out!\nYou LOST! Get Rekt. Better luck in the next life.\n");
            else Console.WriteLine("\nBoth teams got wiped out. How does that even happen? \n------*awkward silence*------\n");
            // NOTE: Take advantage of the Player and Unit object. Call the object's function whenever applicable.
            // NOTE: Please use functions to implement the listed items
        }
    }
}
