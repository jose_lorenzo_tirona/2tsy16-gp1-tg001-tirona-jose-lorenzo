﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tirona
{
    class Program
    {
        static void Main(string[] args)
        {
            string input;
            string[] items = { "Health Potion", "Mana Potion", "Yggdrasil Leaf", "Dreadwyrm Staff", "Holy Robe" };
            Console.WriteLine("What do ya need?");
            input = Console.ReadLine();
            for(int x=0; x<items.Length; x++)
            {
                if (items[x] == input)
                {
                    Console.WriteLine("Found");
                    break;
                }
                else if (x == items.Length-1)
                    Console.WriteLine("Not in array");
            }
        }
    }
}
